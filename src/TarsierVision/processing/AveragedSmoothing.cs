﻿/*
 * This algorithm is based on Karl Sanford's 'Smoothing Kinect Depth Frames in Real-Time',
 * http://www.codeproject.com/Articles/317974/KinectDepthSmoothing
 * The Code Project Open License (CPOL) 1.02
 */


using System.Collections.Generic;
using System.Threading.Tasks;

namespace TarsierVision.Processing
{
    // Reference http://www.codeproject.com/Articles/317974/KinectDepthSmoothing
    public class AveragedSmoothing
    {
        // Will specify how many frames to hold in the Queue for averaging
        private int _averageFrameCount;
        public int AverageFrameCount { get { return _averageFrameCount; } set { if (value > 0 && value <= MaxAverageFrameCount) _averageFrameCount = value; } }
        // The actual Queue that will hold all of the frames to be averaged
        private Queue<ushort[]> averageQueue = new Queue<ushort[]>();

        public static readonly int MaxAverageFrameCount = 20;
        public int width;
        public int height;

        public AveragedSmoothing(int AverageFrameCount, int width, int height)
        {
            this.AverageFrameCount = AverageFrameCount;
            this.width = width;
            this.height = height;
        }

        public ushort[] CreateAverageDepthArray(ushort[] depthArray)
        {
            // This is a method of Weighted Moving Average per pixel coordinate across several frames of depth data.
            // This means that newer frames are linearly weighted heavier than older frames to reduce motion tails,
            // while still having the effect of reducing noise flickering.

            averageQueue.Enqueue(depthArray);
            CheckForDequeue();
            uint[] sumDepthArray = new uint[depthArray.Length];
            ushort[] denominators = new ushort[depthArray.Length];
            for (int i = 0; i < denominators.Length; i++) denominators[i] = 0;
            ushort[] averagedDepthArray = new ushort[depthArray.Length];

            //uint Denominator = 0;
            uint Count = 1;

            // REMEMBER!!! Queue's are FIFO (first in, first out).  This means that when you iterate
            // over them, you will encounter the oldest frame first.

            // We first create a single array, summing all of the pixels of each frame on a weighted basis
            // and determining the denominator that we will be using later.
            foreach (var item in averageQueue)
            {
                // Process each row in parallel
                Parallel.For(0, height, depthArrayRowIndex =>
                {
                    // Process each pixel in the row
                    for (int depthArrayColumnIndex = 0; depthArrayColumnIndex < width; depthArrayColumnIndex++)
                    {
                        var index = depthArrayColumnIndex + (depthArrayRowIndex * width);
                        ushort val = item[index];
                        if (val > ushort.MinValue && val < ushort.MaxValue)
                        {
                            sumDepthArray[index] += item[index] * Count;
                            denominators[index] += (ushort)Count;
                        }
                    }
                });
                //Denominator += Count;
                Count++;
            }

            // Once we have summed all of the information on a weighted basis, we can divide each pixel
            // by our calculated denominator to get a weighted average.

            // Process each row in parallel
            Parallel.For(0, height, depthArrayRowIndex =>
            {
                // Process each pixel in the row
                for (int depthArrayColumnIndex = 0; depthArrayColumnIndex < width; depthArrayColumnIndex++)
                {
                    var index = depthArrayColumnIndex + (depthArrayRowIndex * width);
                    if (denominators[index] > 0)
                    {
                        averagedDepthArray[index] = (ushort)(sumDepthArray[index] / denominators[index]);
                    } else
                    {
                        averagedDepthArray[index] = ushort.MaxValue;
                    }
                }
            });

            return averagedDepthArray;
        }

        public void CheckForDequeue()
        {
            // We will recursively check to make sure we have Dequeued enough frames.
            // This is due to the fact that a user could constantly be changing the UI element
            // that specifies how many frames to use for averaging.
            if (averageQueue.Count > _averageFrameCount)
            {
                averageQueue.Dequeue();
                CheckForDequeue();
            }
        }
    }
}
