﻿/*
 *     __                      .__              
 *   _/  |______ _______  _____|__| ___________ 
 *   \   __\__  \\_  __ \/  ___/  |/ __ \_  __ \
 *    |  |  / __ \|  | \/\___ \|  \  ___/|  | \/
 *    |__| (____  /__|  /____  >__|\___  >__|   
 *              \/           \/        \/       
 * 
 *    Copyright Cameron Newnham 2015-2016
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;

using Grasshopper.Kernel;
using Rhino.Geometry;
using Microsoft.Kinect;
using System.Drawing;
using System.Windows.Forms;
using TarsierVision.Processing;
using System.Drawing.Imaging;

namespace Tarsier.Kinect
{
    [Obsolete]
	public class Kinect2Component_ColorFrame : GH_Component
	{
		public Kinect2Component_ColorFrame()
		  : base("Kinect 2 Color Frame Depth", "Color Frame",
			  "Reads data from a kinect when prompted.",
			  "Tarsier", "Vision")
		{
		}

        public override bool Obsolete => true;

        public override GH_Exposure Exposure
		{
			get { return GH_Exposure.hidden; }
		}

		protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
		{
			pManager.AddBooleanParameter("Enable", "E", "Enable this component. This will turn the kinect on and begin monitoring.", GH_ParamAccess.item, false);
			pManager.AddBooleanParameter("Release", "R", "Release the latest frame. Connect a timer to enable iterative release.", GH_ParamAccess.item, false);
		}

		protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
		{
			pManager.AddGenericParameter("Point Cloud", "C", "The depth buffer as a point cloud.", GH_ParamAccess.item);
		}

		protected override System.Drawing.Bitmap Icon
		{
			get { return TarsierVision.Properties.Resources.icon_kinectv2; }
		}

		public override Guid ComponentGuid
		{
			get { return new Guid("{4276EC23-B22F-4D34-9FEC-B358DD46FBF8}"); }
		}

		private KinectSensor sensor;

		private CoordinateMapper coordinateMapper = null;
		private MultiSourceFrameReader multiFrameSourceReader = null;

		private ushort[] depthFrameData = null;
		private byte[] colorFrameData = null;
		//private ColorSpacePoint[] colorPoints = null;
		//private DepthSpacePoint[] depthSpacePoints = null;
		private uint bytesPerColor = 4;
		private uint bytesPerDepth;
		private CameraSpacePoint[] cameraPoints = null;

		private PointCloud currentPC = new PointCloud();
		private Bitmap currentBMP = null;

		bool depthSet = false;
		int minDepth = 0;
		int maxDepth = 8000;
		int count;

		int depthWidth;
		int depthHeight;

		int colorWidth;
		int colorHeight;

		int prevCount = -1;

		protected override void SolveInstance(IGH_DataAccess DA)
		{
			bool enable = false;
			bool release = false;
			if (!DA.GetData("Enable", ref enable)) return;
			if (!DA.GetData("Release", ref release)) return;

			if (enable)
			{
				sensor = KinectSensor.GetDefault();
				if (!sensor.IsOpen)
				{
					prevCount = -1;
					count = 0;

					multiFrameSourceReader = sensor.OpenMultiSourceFrameReader(FrameSourceTypes.Depth | FrameSourceTypes.Color);

					FrameDescription depthFrameDescription = sensor.DepthFrameSource.FrameDescription;
					FrameDescription colorFrameDescription = sensor.ColorFrameSource.FrameDescription;

					multiFrameSourceReader.MultiSourceFrameArrived += this.Reader_FrameArrived;

					depthWidth = depthFrameDescription.Width;
					depthHeight = depthFrameDescription.Height;
					colorWidth = colorFrameDescription.Width;
					colorHeight = colorFrameDescription.Height;

					depthFrameData = new ushort[depthWidth * depthHeight];
					//colorPoints = new ColorSpacePoint[depthWidth * depthHeight];
					cameraPoints = new CameraSpacePoint[colorWidth * colorHeight];
					//depthSpacePoints = new DepthSpacePoint[colorWidth * colorHeight];

					//bytesPerColor = colorFrameDescription.BytesPerPixel;
					bytesPerDepth = depthFrameDescription.BytesPerPixel;

					colorFrameData = new byte[colorWidth * colorHeight * bytesPerColor];

					coordinateMapper = sensor.CoordinateMapper;
					sensor.Open();
				}
			}
			else if (!enable && sensor != null && sensor.IsOpen)
			{
				sensor.Close();
				multiFrameSourceReader.MultiSourceFrameArrived -= this.Reader_FrameArrived;
				multiFrameSourceReader.Dispose();
				multiFrameSourceReader = null;
				sensor = null;
			}

			if (enable && sensor != null && sensor.IsOpen && release)
			{
				if (count > prevCount)
				{
					prevCount = count;
					currentPC = new PointCloud();

					for (int x = 0; x < colorWidth; x += 1)
					{
						for (int y = 0; y < colorHeight; y += 1)
						{
							int ind = x + y * colorWidth;

							CameraSpacePoint p = this.cameraPoints[ind];
							//ColorSpacePoint c = this.colorPoints[ind];

							// make sure the depth pixel maps to a valid point in color space
							//int colorX = (int)Math.Floor(c.X + 0.5);
							//int colorY = (int)Math.Floor(c.Y + 0.5);
							//byte r = 0; byte g = 0; byte b = 0;
							//if ((colorX >= 0) && (colorX < colorWidth) && (colorY >= 0) && (colorY < colorHeight))
							//{
							int colorIndex = (int) (ind * bytesPerColor);
							byte r = this.colorFrameData[colorIndex++];
							byte g = this.colorFrameData[colorIndex++];
							byte b = this.colorFrameData[colorIndex++];
							//}

							var col = Color.FromArgb((int)r, (int)g, (int)b);
							if (!(Single.IsInfinity(p.X)) && !(Single.IsInfinity(p.Y)) && !(Single.IsInfinity(p.Z)))
							{
								currentPC.Add(new Point3d(p.X, p.Y, p.Z), col);
							}
							else
							{
								currentPC.Add(Point3d.Origin, col);
							}
						}
					}
				}
				DA.SetData("Point Cloud", currentPC);
			}
		}

		private bool inProcess = false;

		private void Reader_FrameArrived(object sender, MultiSourceFrameArrivedEventArgs e)
		{
			if (!inProcess)
			{
				inProcess = true;
				count++;
				this.Message = count.ToString();

				MultiSourceFrame multiSourceFrame = e.FrameReference.AcquireFrame();

				if (multiSourceFrame != null)
				{
					using (DepthFrame depthFrame = multiSourceFrame.DepthFrameReference.AcquireFrame())
					{
						using (ColorFrame colorFrame = multiSourceFrame.ColorFrameReference.AcquireFrame())
						{
							if (depthFrame != null)
							{
								FrameDescription depthFrameDescription = depthFrame.FrameDescription;

								if ((depthWidth * depthHeight) == this.depthFrameData.Length)
								{
									depthFrame.CopyFrameDataToArray(this.depthFrameData);
								}
							}

							if (colorFrame != null)
							{
								FrameDescription colorFrameDescription = colorFrame.FrameDescription;
								if ((colorWidth * colorHeight * bytesPerColor) == this.colorFrameData.Length)
								{
									colorFrame.CopyConvertedFrameDataToArray(this.colorFrameData, ColorImageFormat.Rgba);
								}
							}
						}
					}
				}

				//this.coordinateMapper.MapDepthFrameToColorSpace(this.depthFrameData, this.colorPoints);
				//this.coordinateMapper.MapDepthFrameToCameraSpace(this.depthFrameData, this.cameraPoints);

				//this.coordinateMapper.MapColorFrameToDepthSpace(this.depthFrameData, this.depthSpacePoints);
				this.coordinateMapper.MapColorFrameToCameraSpace(this.depthFrameData, this.cameraPoints);
				//this.coordinateMapper.MapDepthPointsToCameraSpace(this.depthSpacePoints, )

				inProcess = false;

			}
		}
	}
}
