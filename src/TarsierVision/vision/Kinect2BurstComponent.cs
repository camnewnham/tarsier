﻿/*
 *     __                      .__              
 *   _/  |______ _______  _____|__| ___________ 
 *   \   __\__  \\_  __ \/  ___/  |/ __ \_  __ \
 *    |  |  / __ \|  | \/\___ \|  \  ___/|  | \/
 *    |__| (____  /__|  /____  >__|\___  >__|   
 *              \/           \/        \/       
 * 
 *    Copyright Cameron Newnham 2015-2016
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;

using Grasshopper.Kernel;
using Rhino.Geometry;
using Microsoft.Kinect;
using System.Drawing;
using System.Windows.Forms;
using TarsierVision.Processing;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Threading;

namespace Tarsier.Kinect
{
    public class Kinect2BurstComponent : GH_Component
    {
        public Kinect2BurstComponent()
          : base("Kinect 2 Burst", "Burst",
              "Captures a burst of frames at maximum rate, releasing after the allotted timeframe",
              "Tarsier", "Vision")
        {
        }

        public override GH_Exposure Exposure
        {
            get { return GH_Exposure.tertiary; }
        }

        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddBooleanParameter("Enable", "E", "Enable this component. Use a toggle. This will turn the kinect on and begin monitoring.", GH_ParamAccess.item, false);
            pManager.AddBooleanParameter("Capture", "C", "Start the burst capture. Use a button.", GH_ParamAccess.item, false);
            pManager.AddNumberParameter("Duration", "D", "The duration to capture, in seconds. Between 0.02 and 20", GH_ParamAccess.item, 1);
        }

        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddGenericParameter("Burst Frames", "B", "The burst frames as a list of point clouds.", GH_ParamAccess.list);
        }

        protected override System.Drawing.Bitmap Icon
        {
            get { return TarsierVision.Properties.Resources.icon_kinectv2; }
        }

        public override Guid ComponentGuid
        {
            get { return new Guid("{69F51828-7C1C-4060-A815-65198A4C4EC6}"); }
        }

        private KinectSensor sensor;

        private CoordinateMapper coordinateMapper = null;
        private MultiSourceFrameReader multiFrameSourceReader = null;

        private const uint bytesPerColor = 4;
        private uint bytesPerDepth;

        int count;

        private struct SensorConfig
        {
            public int depthWidth;
            public int depthHeight;
            public int colorWidth;
            public int colorHeight;
        }

        private SensorConfig sensorConfig;

        private bool capturing = false;

        private List<PointCloud> burstResult = new List<PointCloud>();

        protected override void SolveInstance(IGH_DataAccess DA)
        {
            bool enable = false;
            bool capture = false;
            double duration = 1;
            if (!DA.GetData("Enable", ref enable)) return;
            if (!DA.GetData("Capture", ref capture)) return;
            DA.GetData("Duration", ref duration);

            if (enable)
            {
                sensor = KinectSensor.GetDefault();
                if (!sensor.IsOpen)
                {
                    count = 0;

                    multiFrameSourceReader = sensor.OpenMultiSourceFrameReader(FrameSourceTypes.Depth | FrameSourceTypes.Color);

                    FrameDescription depthFrameDescription = sensor.DepthFrameSource.FrameDescription;
                    FrameDescription colorFrameDescription = sensor.ColorFrameSource.FrameDescription;

                    multiFrameSourceReader.MultiSourceFrameArrived += this.Reader_FrameArrived;

                    sensorConfig = new SensorConfig();
                    sensorConfig.depthWidth = depthFrameDescription.Width;
                    sensorConfig.depthHeight = depthFrameDescription.Height;
                    sensorConfig.colorWidth = colorFrameDescription.Width;
                    sensorConfig.colorHeight = colorFrameDescription.Height;

                    //bytesPerColor = colorFrameDescription.BytesPerPixel;
                    bytesPerDepth = depthFrameDescription.BytesPerPixel;


                    coordinateMapper = sensor.CoordinateMapper;
                    sensor.Open();
                }
            }
            else if (!enable && sensor != null && sensor.IsOpen)
            {
                sensor.Close();
                multiFrameSourceReader.MultiSourceFrameArrived -= this.Reader_FrameArrived;
                multiFrameSourceReader.Dispose();
                multiFrameSourceReader = null;
                sensor = null;
            }

            if (enable && sensor != null && sensor.IsOpen && capture)
            {
                if (!capturing)
                {
                    // Start clock, being capturing
                    if (duration < 0.02)
                    {
                        AddRuntimeMessage(GH_RuntimeMessageLevel.Error, "Duration can not be less than 20 ms");
                        return;
                    }

                    if (duration > 20)
                    {
                        AddRuntimeMessage(GH_RuntimeMessageLevel.Error, "Duration can not be larger than 20 seconds, as you may run out of memory. If you need this, please edit the source code or contact the developer. You can always just use a timer and data recorder.");
                        return;
                    }

                    capturing = true;
                    count = 0;
                    lock (burstFrames)
                    {
                        burstFrames.Clear();
                    }

                    Task.Factory.StartNew(() =>
                    {
                        // Wait for the delay
                        Thread.Sleep((int) TimeSpan.FromSeconds(duration).TotalMilliseconds-5);
                        capturing = false;

                        OnPingDocument()?.ScheduleSolution(5, (d) =>
                        {
                            lock (burstFrames)
                            {
                                lock (burstResult)
                                {
                                    burstResult.Clear();
                                    foreach (var burstFrame in burstFrames)
                                    {
                                        var pc = BuildPointCloud(burstFrame, sensorConfig);
                                        if (pc?.Count > 0)
                                        {
                                            burstResult.Add(BuildPointCloud(burstFrame, sensorConfig));
                                        }
                                    }
                                }
                            }
                            ExpireSolution(false);
                        });
                    });
                }
            }
            DA.SetDataList("Burst Frames", burstResult);
        }

        private List<FrameData> burstFrames = new List<FrameData>();

        struct FrameData
        {
            public ColorSpacePoint[] colorPoints;
            public CameraSpacePoint[] cameraPoints;
            public byte[] colorFrameData;
        }

        private static Rhino.Geometry.PointCloud BuildPointCloud(FrameData data, SensorConfig config)
        {
            PointCloud pc = new PointCloud();

            for (int x = 0; x < config.depthWidth; x++)
            {
                for (int y = 0; y < config.depthHeight; y++)
                {
                    int ind = x + y * config.depthWidth;

                    CameraSpacePoint p = data.cameraPoints[ind];
                    ColorSpacePoint c = data.colorPoints[ind];

                    // make sure the depth pixel maps to a valid point in color space
                    int colorX = (int)Math.Floor(c.X + 0.5);
                    int colorY = (int)Math.Floor(c.Y + 0.5);
                    byte r = 0; byte g = 0; byte b = 0;
                    if ((colorX >= 0) && (colorX < config.colorWidth) && (colorY >= 0) && (colorY < config.colorHeight))
                    {
                        int colorIndex = (int)(((colorY * config.colorWidth) + colorX) * bytesPerColor);
                        r = data.colorFrameData[colorIndex++];
                        g = data.colorFrameData[colorIndex++];
                        b = data.colorFrameData[colorIndex++];
                    }

                    if (Single.IsInfinity(p.X)) continue;
                    if (Single.IsInfinity(p.Y)) continue;
                    if (Single.IsInfinity(p.Z)) continue;

                    var col = Color.FromArgb((int)r, (int)g, (int)b);
                    pc.Add(new Point3d(p.X, p.Y, p.Z), col);
                }
            }

            return pc;
        }

        private bool inProcess = false;

        private void Reader_FrameArrived(object sender, MultiSourceFrameArrivedEventArgs e)
        {
            if (!inProcess && capturing) {
                inProcess = true;
                count++;
                Message = count.ToString();
                if (count % 5 == 0) Grasshopper.Instances.ActiveCanvas.Invalidate();

                ushort[]  depthFrameData = new ushort[sensorConfig.depthWidth * sensorConfig.depthHeight];
                byte[] colorFrameData = new byte[sensorConfig.colorWidth * sensorConfig.colorHeight * bytesPerColor]; 
                ColorSpacePoint[]  colorPoints = new ColorSpacePoint[sensorConfig.depthWidth * sensorConfig.depthHeight]; 
                CameraSpacePoint[] cameraPoints = new CameraSpacePoint[sensorConfig.depthWidth * sensorConfig.depthHeight];

                MultiSourceFrame multiSourceFrame = e.FrameReference.AcquireFrame();

                if (multiSourceFrame != null)
                {
                    using (DepthFrame depthFrame = multiSourceFrame.DepthFrameReference.AcquireFrame())
                    {
                        using (ColorFrame colorFrame = multiSourceFrame.ColorFrameReference.AcquireFrame())
                        {
                            if (depthFrame != null)
                            {
                                FrameDescription depthFrameDescription = depthFrame.FrameDescription;

                                if ((sensorConfig.depthWidth * sensorConfig.depthHeight) == depthFrameData.Length)
                                {
                                    depthFrame.CopyFrameDataToArray(depthFrameData);
                                }
                            }
                            
                            if (colorFrame != null)
                            {
                                FrameDescription colorFrameDescription = colorFrame.FrameDescription;
                                if ((sensorConfig.colorWidth * sensorConfig.colorHeight * bytesPerColor) == colorFrameData.Length)
                                {
                                    colorFrame.CopyConvertedFrameDataToArray(colorFrameData, ColorImageFormat.Rgba);
                                }
                            }
                        }
                    }
                }

                this.coordinateMapper.MapDepthFrameToColorSpace(depthFrameData, colorPoints);
                this.coordinateMapper.MapDepthFrameToCameraSpace(depthFrameData, cameraPoints);

                lock (burstFrames)
                {
                    burstFrames.Add(new FrameData()
                    {
                        cameraPoints = cameraPoints,
                        colorFrameData = colorFrameData,
                        colorPoints = colorPoints
                    });
                }

                inProcess = false;
                
            }
        }
    }
}
