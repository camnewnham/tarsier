﻿/*
 *     __                      .__              
 *   _/  |______ _______  _____|__| ___________ 
 *   \   __\__  \\_  __ \/  ___/  |/ __ \_  __ \
 *    |  |  / __ \|  | \/\___ \|  \  ___/|  | \/
 *    |__| (____  /__|  /____  >__|\___  >__|   
 *              \/           \/        \/       
 * 
 *    Copyright Cameron Newnham 2015-2016
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;

using Grasshopper.Kernel;
using System.Linq;
using Grasshopper.Kernel.Types;
using TarsierGH;

namespace Tarsier.Tools
{
    public class DeconstructPointCloudComponent : GH_Component
    {
        public DeconstructPointCloudComponent()
          : base("Deconstruct Point Cloud", "Deconstruct",
              "Retreives information from a point cloud.",
              "Tarsier", "Creation")
        {
        }

        public override GH_Exposure Exposure
        {
            get
            {
                return GH_Exposure.primary;
            }
        }

        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddParameter(new GH_PointCloudParam(), "Point Cloud", "C", "The point cloud to deconstruct", GH_ParamAccess.item);
        }

        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddPointParameter("Points", "P", "The points representing the point cloud", GH_ParamAccess.list);
            pManager.AddColourParameter("Colours", "C", "Colours for each point.", GH_ParamAccess.list);
            pManager.AddVectorParameter("Normals", "N", "Vectors representing the normals of each point.", GH_ParamAccess.list);
        }

        protected override void SolveInstance(IGH_DataAccess DA)
        {
            GH_PointCloud pc = null;
            if (!DA.GetData("Point Cloud", ref pc)) return;

            if (pc.Value.Count == 0)
            {
                AddRuntimeMessage(GH_RuntimeMessageLevel.Warning, "No points were found.");
            }
            else
            {
                DA.SetDataList("Points", pc.Value.GetPoints().Select(x => new GH_Point(x)));
                if (pc.Value.ContainsColors)
                {
                    DA.SetDataList("Colours", pc.Value.GetColors().Select(x => new GH_Colour(x)));
                }
                if (pc.Value.ContainsNormals)
                {
                    DA.SetDataList("Normals", pc.Value.GetNormals().Select(x => new GH_Vector(x)));
                }
            }
        }

        protected override System.Drawing.Bitmap Icon
        {
            get
            {
                return Tarsier.Properties.Resources.icon_deconstructpointcloud;
            }
        }

        public override Guid ComponentGuid
        {
            get { return new Guid("{920780C9-05CF-46FC-9C7D-1FFBD7A879AF}"); }
        }
    }
}
