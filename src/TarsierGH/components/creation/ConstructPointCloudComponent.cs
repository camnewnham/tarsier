﻿/*
 *     __                      .__              
 *   _/  |______ _______  _____|__| ___________ 
 *   \   __\__  \\_  __ \/  ___/  |/ __ \_  __ \
 *    |  |  / __ \|  | \/\___ \|  \  ___/|  | \/
 *    |__| (____  /__|  /____  >__|\___  >__|   
 *              \/           \/        \/       
 * 
 *    Copyright Cameron Newnham 2015-2016
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;

using Grasshopper.Kernel;
using Rhino.Geometry;
using System.Drawing;
using TarsierGH;

namespace Tarsier.Tools
{
    public class ConstructPointCloudComponent : GH_Component
    {
        public ConstructPointCloudComponent()
          : base("Construct Point Cloud", "Construct",
              "Constructs a point cloud from a list of points and colours.",
              "Tarsier", "Creation")
        {
        }

        public override GH_Exposure Exposure
        {
            get
            {
                return GH_Exposure.primary;
            }
        }

        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddPointParameter("Points", "P", "The points representing the point cloud", GH_ParamAccess.list);
            pManager.AddColourParameter("Colours", "C", "Colours for each point.", GH_ParamAccess.list);
            pManager.AddVectorParameter("Normals", "N", "Vectors representing the normals of each point.", GH_ParamAccess.list);
            pManager[1].Optional = true;
            pManager[2].Optional = true;
        }

        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddParameter(new GH_PointCloudParam(), "Point Cloud", "C", "The created point cloud", GH_ParamAccess.item);
        }

        protected override void SolveInstance(IGH_DataAccess DA)
        {
            List<Point3d> pts = new List<Point3d>();
            List<Color> colors = new List<Color>();
            List<Vector3d> normals = new List<Vector3d>();
            if (!DA.GetDataList("Points", pts)) return;
            DA.GetDataList("Colours", colors);
            DA.GetDataList("Normals", normals);

            if (colors.Count > 0 && colors.Count != pts.Count)
                AddRuntimeMessage(GH_RuntimeMessageLevel.Warning, "Colours should be empty or match the number of points.");
            if (normals.Count > 0 && normals.Count != pts.Count)
                AddRuntimeMessage(GH_RuntimeMessageLevel.Warning, "Normals should be empty or match the number of points.");

            bool usingNormals = normals.Count == pts.Count;
            bool usingColors = colors.Count == pts.Count;

            PointCloud pc = new PointCloud();

            if (!usingNormals && !usingColors)
            {
                pc.AddRange(pts);
            }
            else
            {
                for (int i = 0; i < pts.Count; i++)
                {
                    if (usingColors && usingNormals)
                    {
                        pc.Add(pts[i], normals[i], colors[i]);
                    }
                    else if (usingColors)
                    {
                        pc.Add(pts[i], colors[i]);
                    }
                    else if (usingNormals)
                    {
                        pc.Add(pts[i], normals[i]);
                    }
                }
            }

            DA.SetData("Point Cloud", new GH_PointCloud(pc));
        }

        protected override System.Drawing.Bitmap Icon
        {
            get
            {
                return Tarsier.Properties.Resources.icon_constructpointcloud;
            }
        }

        public override Guid ComponentGuid
        {
            get { return new Guid("{0d021e3f-0fdd-4bdb-ae1c-ad2b9fba9273}"); }
        }
    }
}
