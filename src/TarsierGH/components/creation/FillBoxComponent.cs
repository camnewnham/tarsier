﻿using System;
using System.Collections.Generic;

using Grasshopper.Kernel;
using Rhino.Geometry;
using System.Drawing;
using TarsierGH;
using System.Linq;
using System.Windows.Forms;
using System.Threading.Tasks;

namespace Tarsier.Tools
{
    public class FillBoxComponent : GH_Component
    {
        public FillBoxComponent()
          : base("Fill Box", "FillBox",
              "Fills a box with random points.",
              "Tarsier", "Creation")
        {
        }

        public override GH_Exposure Exposure
        {
            get
            {
                return GH_Exposure.tertiary;
            }
        }

        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddBoxParameter("Box", "B", "The box to fill.", GH_ParamAccess.item);
            pManager.AddIntegerParameter("Amount", "N", "The number of points to add.", GH_ParamAccess.item, 100);
            pManager.AddIntegerParameter("Seed", "S", "The random seed.", GH_ParamAccess.item, 2);
            pManager[1].Optional = true;
            pManager[2].Optional = true;
        }

        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddParameter(new GH_PointCloudParam(), "Point Cloud", "C", "The generated point cloud.", GH_ParamAccess.item);
        }


        protected override void SolveInstance(IGH_DataAccess DA)
        {
            int numPts = 100;
            int seed = 2;
            Box box = Box.Unset;

            if (!DA.GetData("Box", ref box)) return;
            DA.GetData("Amount", ref numPts);
            DA.GetData("Seed", ref seed);

            Random rng = new Random(seed);
            PointCloud pc = new PointCloud();

            box.RepositionBasePlane(box.Center);

            var xfm = Transform.PlaneToPlane(Plane.WorldXY, box.Plane) * Transform.Scale(Plane.WorldXY, box.X.Max - box.X.Min, box.Y.Max - box.Y.Min, box.Z.Max - box.Z.Min);

            for (int i=0; i<numPts; i++) pc.Add(new Point3d(rng.NextDouble() - 0.5d, rng.NextDouble() - 0.5d, rng.NextDouble() - 0.5d));
            pc.Transform(xfm);
            DA.SetData("Point Cloud", new GH_PointCloud(pc));
        }

        protected override System.Drawing.Bitmap Icon
        {
            get
            {
                return Tarsier.Properties.Resources.icon_fillbox;
            }
        }

        public override Guid ComponentGuid
        {
            get { return new Guid("{165E2746-7211-49FD-B058-0BF39B735F2D}"); }
        }
    }
}
