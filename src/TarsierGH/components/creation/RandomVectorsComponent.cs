﻿/*
 *     __                      .__              
 *   _/  |______ _______  _____|__| ___________ 
 *   \   __\__  \\_  __ \/  ___/  |/ __ \_  __ \
 *    |  |  / __ \|  | \/\___ \|  \  ___/|  | \/
 *    |__| (____  /__|  /____  >__|\___  >__|   
 *              \/           \/        \/       
 * 
 *    Copyright Cameron Newnham 2015-2016
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;

using Grasshopper.Kernel;
using Rhino.Geometry;
using System.Linq;
using Grasshopper.Kernel.Types;

namespace Tarsier.Tools
{
    public class RandomVectorsComponent : GH_Component
    {
        public RandomVectorsComponent()
          : base("Random Vectors", "RandVec",
              "Generates a set of random vectors between (-1,-1,-1) and (1,1,1) .",
              "Tarsier", "Creation")
        {
        }

        public override GH_Exposure Exposure
        {
            get
            {
                return GH_Exposure.tertiary;
            }
        }

        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddIntegerParameter("Amount", "N", "The number of vectors to generate.", GH_ParamAccess.item);
            pManager.AddBooleanParameter("Unitize", "U", "Unitize the output vectors.", GH_ParamAccess.item, true);
            pManager.AddIntegerParameter("Seed", "S", "The random seed.", GH_ParamAccess.item, 2);
            pManager[1].Optional = true;
            pManager[2].Optional = true;
        }

        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddVectorParameter("Random Vectors", "V", "The randomly generated vectors", GH_ParamAccess.list);
        }


        protected override void SolveInstance(IGH_DataAccess DA)
        {
            bool unitize = true;
            int num = 100;
            int seed = 2;

            if (!DA.GetData("Amount", ref num)) return;
            DA.GetData("Unitize", ref unitize);
            DA.GetData("Seed", ref seed);


            Random rng = new Random(2);

            List<Vector3d> vecs = new List<Vector3d>();

            for (int i=0; i< num; i++)
            {
                Vector3d vec = new Vector3d(rng.NextDouble() * 2 - 1, rng.NextDouble() * 2 - 1, rng.NextDouble() * 2 - 1);
                if (unitize) vec.Unitize();
                vecs.Add(vec);
            }

            DA.SetDataList("Random Vectors", vecs.Select(x => new GH_Vector(x)));

        }

        protected override System.Drawing.Bitmap Icon
        {
            get
            {
                return Tarsier.Properties.Resources.icon_randomvector;
            }
        }

        public override Guid ComponentGuid
        {
            get { return new Guid("{74DFE144-A570-47FF-810E-8410C49EEB23}"); }
        }
    }
}
