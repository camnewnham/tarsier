﻿/*
 *     __                      .__              
 *   _/  |______ _______  _____|__| ___________ 
 *   \   __\__  \\_  __ \/  ___/  |/ __ \_  __ \
 *    |  |  / __ \|  | \/\___ \|  \  ___/|  | \/
 *    |__| (____  /__|  /____  >__|\___  >__|   
 *              \/           \/        \/       
 * 
 *    Copyright Cameron Newnham 2015-2016
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;

using Grasshopper.Kernel;
using Rhino.Geometry;
using TarsierGH;

namespace Tarsier.Tools
{
    public class FillMeshComponent : GH_Component
    {
        public FillMeshComponent()
          : base("Fill Mesh", "FillMesh",
              "Fills a mesh volume with random points.",
              "Tarsier", "Creation")
        {
        }

        public override GH_Exposure Exposure
        {
            get
            {
                return GH_Exposure.tertiary;
            }
        }

        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddMeshParameter("Mesh", "M", "The mesh to populate.", GH_ParamAccess.item);
            pManager.AddIntegerParameter("Amount", "N", "The number of points to add.", GH_ParamAccess.item, 100);
            pManager.AddIntegerParameter("Seed", "S", "The random seed.", GH_ParamAccess.item, 2);
            pManager[1].Optional = true;
            pManager[2].Optional = true;
        }

        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddParameter(new GH_PointCloudParam(), "Point Cloud", "C", "The generated point cloud.", GH_ParamAccess.item);
        }


        protected override void SolveInstance(IGH_DataAccess DA)
        {
            int numPts = 100;
            int seed = 2;
            Mesh mesh = null;
            if (!DA.GetData("Mesh", ref mesh)) return;
            DA.GetData("Amount", ref numPts);
            DA.GetData("Seed", ref seed);

            Random rng = new Random(seed);
            PointCloud pc = new PointCloud();

            BoundingBox box = mesh.GetBoundingBox(false);
            Point3d offset = box.Corner(true, true, true);
            Point3d range = new Point3d(box.Corner(false, false, false) - offset);

            while (pc.Count < numPts)
            {
                Point3d newPt = new Point3d(rng.NextDouble() * range.X + offset.X, rng.NextDouble() * range.Y + offset.Y, rng.NextDouble() * range.Z + offset.Z);
                if (mesh.IsPointInside(newPt, Rhino.RhinoDoc.ActiveDoc.ModelAbsoluteTolerance, false))
                {
                    pc.Add(newPt);
                }
            }

            DA.SetData("Point Cloud", new GH_PointCloud(pc));
        }

        protected override System.Drawing.Bitmap Icon
        {
            get
            {
                return Tarsier.Properties.Resources.icon_fillmesh;
            }
        }

        public override Guid ComponentGuid
        {
            get { return new Guid("{0EC7C5A2-2A7C-426A-892F-73BE90B39939}"); }
        }
    }
}
