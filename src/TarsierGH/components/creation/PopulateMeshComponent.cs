﻿/*
 *     __                      .__              
 *   _/  |______ _______  _____|__| ___________ 
 *   \   __\__  \\_  __ \/  ___/  |/ __ \_  __ \
 *    |  |  / __ \|  | \/\___ \|  \  ___/|  | \/
 *    |__| (____  /__|  /____  >__|\___  >__|   
 *              \/           \/        \/       
 * 
 *    Copyright Cameron Newnham 2015-2016
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;

using Grasshopper.Kernel;
using Rhino.Geometry;
using TarsierGH;
using System.Linq;

namespace Tarsier.Tools
{
    public class PopulateMeshComponent : GH_Component
    {
        public PopulateMeshComponent()
          : base("Populate Mesh", "PopMesh",
              "Populates a mesh with random points.",
              "Tarsier", "Creation")
        {
        }

        public override GH_Exposure Exposure
        {
            get
            {
                return GH_Exposure.tertiary;
            }
        }

        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddMeshParameter("Mesh", "M", "The mesh to populate.", GH_ParamAccess.item);
            pManager.AddIntegerParameter("Amount", "N", "The number of points to add.", GH_ParamAccess.item, 100);
            pManager.AddBooleanParameter("Normals", "N", "Include face normals in the point cloud", GH_ParamAccess.item, false);
            pManager.AddIntegerParameter("Seed", "S", "The random seed.", GH_ParamAccess.item, 2);
            pManager[1].Optional = true;
            pManager[2].Optional = true;
            pManager[3].Optional = true;
        }

        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddParameter(new GH_PointCloudParam(), "Point Cloud", "C", "The generated point cloud.", GH_ParamAccess.item);
        }


        protected override void SolveInstance(IGH_DataAccess DA)
        {
            Mesh mesh = null;
            int numPts = 100;
            int seed = 2;
            bool includeNormals = false;

            if (!DA.GetData("Mesh", ref mesh)) return;
            DA.GetData("Amount", ref numPts);
            DA.GetData("Seed", ref seed);
            DA.GetData("Normals", ref includeNormals);


            PointCloud pc = new PointCloud();

            mesh.Faces.ConvertQuadsToTriangles();
            mesh.FaceNormals.ComputeFaceNormals();

            AreaMassProperties amp = AreaMassProperties.Compute(mesh);
            double area = amp.Area;

            double areaPerPt = area / (numPts+1);

            rng = new Random(seed);
            List<int> newOrder = Enumerable.Range(0, mesh.Faces.Count).ToList();
            Shuffle(newOrder);

            double remainder = 0;
            int count = 0;

            for (int i = 0; i < newOrder.Count; i++)
            {
                int faceIndex = newOrder[i];
                MeshFace f = mesh.Faces[faceIndex];
                double faceArea = TriangleArea(mesh.Vertices[f.A], mesh.Vertices[f.B], mesh.Vertices[f.C]);
                faceArea += remainder;
                Vector3d norm = mesh.FaceNormals[faceIndex];

                while (faceArea > areaPerPt && count < numPts)
                {
                    double b0 = rng.NextDouble();
                    double b1 = rng.NextDouble();

                    if (b0 + b1 > 1)
                    {
                        b0 = 1 - b0;
                        b1 = 1 - b1;
                    }

                    double b2 = 1 - b0 - b1;
                    if (includeNormals)
                    {
                        pc.Add(mesh.PointAt(faceIndex, b0, b1, b2, 0), norm);
                    } else
                    {
                        pc.Add(mesh.PointAt(faceIndex, b0, b1, b2, 0));
                    }
                    count++;
                    faceArea -= areaPerPt;
                }
                remainder = faceArea;
            }

            DA.SetData("Point Cloud", new GH_PointCloud(pc));
        }

        Random rng;

        protected override System.Drawing.Bitmap Icon
        {
            get
            {
                return Tarsier.Properties.Resources.icon_populatemesh;
            }
        }



        public double TriangleArea(Point3d A, Point3d B, Point3d C)
        {
            double a = new Vector3d(A - B).Length;
            double b = new Vector3d(B - C).Length;
            double c = new Vector3d(C - A).Length;

            return 0.25 * (Math.Sqrt(4 * a * a * b * b - (a * a + b * b - c * c) * (a * a + b * b - c * c)));
        }

        public void Shuffle<T>(IList<T> list)
        {
            int n = list.Count;
            while (n > 1)
            {
                n--;
                int k = rng.Next(n + 1);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }

        public override Guid ComponentGuid
        {
            get { return new Guid("{BD484DB5-354F-4074-BC00-AE44C6A81264}"); }
        }
    }
}
