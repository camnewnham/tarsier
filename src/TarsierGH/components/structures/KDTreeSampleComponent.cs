﻿/*
 *     __                      .__              
 *   _/  |______ _______  _____|__| ___________ 
 *   \   __\__  \\_  __ \/  ___/  |/ __ \_  __ \
 *    |  |  / __ \|  | \/\___ \|  \  ___/|  | \/
 *    |__| (____  /__|  /____  >__|\___  >__|   
 *              \/           \/        \/       
 * 
 *    Copyright Cameron Newnham 2015-2016
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;

using Grasshopper.Kernel;
using Rhino.Geometry;
using Grasshopper.Kernel.Data;
using Grasshopper.Kernel.Types;
using KDTree;
using System.Linq;
using Tarsier.Algorithms;

namespace Tarsier.Tools
{
    public class KDTreeSampleComponent : GH_Component
    {
        public KDTreeSampleComponent()
          : base("Sample KDTree", "KDSample",
              "Builds and/or samples a KDTree representation of a point cloud.",
              "Tarsier", "Searching")
        {
        }

        public override GH_Exposure Exposure
        {
            get
            {
                return GH_Exposure.secondary;
            }
        }

        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddGenericParameter("KD-Tree", "K", "The KD-Tree to sample.", GH_ParamAccess.item);
            pManager.AddPointParameter("Points", "P", "The points to sample.", GH_ParamAccess.list);
            pManager.AddNumberParameter("Range", "R", "The search range.", GH_ParamAccess.list);
            pManager[pManager.AddIntegerParameter("Maximum Points Returned", "N", "The maximum number of points to be returned for each query point.", GH_ParamAccess.item, 1024)].Optional = true;
        }

        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddIntegerParameter("Indices", "I", "The indices of the neighbouring points.", GH_ParamAccess.tree);
        }

        protected override void SolveInstance(IGH_DataAccess DA)
        {
            KDTree<int> tree = null;
            List<Point3d> pts = new List<Point3d>();
            List<double> range = new List<double>();
            int maxReturned = 1024;
            
            if (!DA.GetData("KD-Tree", ref tree)) return;
            if (!DA.GetDataList("Points", pts)) return;
            if (!DA.GetDataList("Range", range)) return;
            DA.GetData("Maximum Points Returned", ref maxReturned);

            if (range.Count == 1)
            {
                for (int i=1; i<pts.Count; i++)
                {
                    range.Add(range[0]);
                }
            } else if (range.Count > 1 && range.Count < pts.Count)
            {
                AddRuntimeMessage(GH_RuntimeMessageLevel.Error, "Range count should either be 1, or match the point count.");
                return;
            }

            List<int>[] inds = SpatialTools.KDTreeMultiSearch(pts, tree, range, maxReturned);

            var path1 = DA.ParameterTargetPath(0);
            GH_Structure<GH_Integer> indStruct = new GH_Structure<GH_Integer>();
            for (int p = 0; p < pts.Count; p++)
            {
                List<int> ids = inds[p];
                indStruct.AppendRange(ids.Select(x => new GH_Integer(x)), path1.AppendElement(p));
            }

            DA.SetDataTree(0, indStruct);
        }



        protected override System.Drawing.Bitmap Icon
        {
            get
            {
                return Tarsier.Properties.Resources.icon_samplekdtree;
            }
        }

        public override Guid ComponentGuid
        {
            get { return new Guid("{E4E3A6CD-C4A4-4ED0-BBF4-E90F0303E1FA}"); }
        }
    }
}
