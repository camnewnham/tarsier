﻿/*
 *     __                      .__              
 *   _/  |______ _______  _____|__| ___________ 
 *   \   __\__  \\_  __ \/  ___/  |/ __ \_  __ \
 *    |  |  / __ \|  | \/\___ \|  \  ___/|  | \/
 *    |__| (____  /__|  /____  >__|\___  >__|   
 *              \/           \/        \/       
 * 
 *    Copyright Cameron Newnham 2015-2016
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;

using Grasshopper.Kernel;
using Rhino.Geometry;
using Grasshopper.Kernel.Types;
using Grasshopper.Kernel.Parameters;
using System.Threading.Tasks;
using System.Collections.Concurrent;

namespace Tarsier.Tools
{
    public class ConeOfVisionComponent : GH_Component
    {
        public ConeOfVisionComponent()
          : base("Field Of Vision", "FOV",
              "Checks whether points are within the field of vision of a point and vector.",
              "Tarsier", "Searching")
        {
        }

        public override GH_Exposure Exposure
        {
            get
            {
                return GH_Exposure.tertiary;
            }
        }

        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddPointParameter("Location", "P", "The location of the search point.", GH_ParamAccess.item);
            pManager.AddVectorParameter("Direction", "V", "The cone direction.", GH_ParamAccess.item);
            degParam = pManager.AddAngleParameter("Field of Vision", "R", "The field of vision.", GH_ParamAccess.item);
            pManager.AddPointParameter("Neighbouring Points", "N", "The points to query.", GH_ParamAccess.list);
        }

        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddBooleanParameter("States", "S", "The state of the point. True=Inside, False=Outside.", GH_ParamAccess.tree);
        }

        int degParam;

        protected override void SolveInstance(IGH_DataAccess DA)
        {
            Point3d loc = Point3d.Unset;
            Vector3d dir = Vector3d.Unset;
            double fov = 0;
            List<Point3d> pts = new List<Point3d>();
            
            if (!DA.GetData("Location", ref loc)) return;
            if (!DA.GetData("Direction", ref dir)) return;
            if (!DA.GetData("Field of Vision", ref fov)) return;
            if (!DA.GetDataList("Neighbouring Points", pts)) return;

            if (((Param_Number)this.Params.Input[degParam]).UseDegrees) fov *= Math.PI / 180; 

            GH_Boolean[] states = new GH_Boolean[pts.Count];

            Parallel.ForEach(Partitioner.Create(0, states.Length), range => {
                for (int x = range.Item1; x < range.Item2; x++)
                    states[x] = new GH_Boolean(IsInFov(loc, dir, fov, pts[x]));
            });


            DA.SetDataList("States", states);
        }


        public bool IsInFov(Point3d loc, Vector3d dir, double fov, Point3d testPt)
        {
            double ang = Vector3d.VectorAngle(dir, testPt - loc);
            return ang < fov / 2;
        }


        protected override System.Drawing.Bitmap Icon
        {
            get
            {
                return Tarsier.Properties.Resources.icon_fieldofview;
            }
        }

        public override Guid ComponentGuid
        {
            get { return new Guid("{5F1C71C0-9518-4911-A6D3-5F2667E27D4A}"); }
        }
    }
}
