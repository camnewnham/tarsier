﻿/*
 *     __                      .__              
 *   _/  |______ _______  _____|__| ___________ 
 *   \   __\__  \\_  __ \/  ___/  |/ __ \_  __ \
 *    |  |  / __ \|  | \/\___ \|  \  ___/|  | \/
 *    |__| (____  /__|  /____  >__|\___  >__|   
 *              \/           \/        \/       
 * 
 *    Copyright Cameron Newnham 2015-2016
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Grasshopper.Kernel;
using Rhino.Geometry;
using System;
using TarsierGH;

namespace Tarsier.Tools
{
    public class ReducePointCloudComponent : GH_Component
    {
        public ReducePointCloudComponent()
          : base("Reduce Point Cloud", "Cloud Reduce",
              "Reduces the number of points in a cloud.",
              "Tarsier", "Modify")
        {
        }

        public override GH_Exposure Exposure
        {
            get
            {
                return GH_Exposure.quinary;
            }
        }

        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddParameter(new GH_PointCloudParam(), "Point Cloud", "C", "The point cloud to reduce.", GH_ParamAccess.item);
            pManager.AddNumberParameter("Fraction", "F", "The percentage reduction. (0-1)", GH_ParamAccess.item);
            pManager[pManager.AddIntegerParameter("Seed", "S", "The random seed.", GH_ParamAccess.item, 2)].Optional=true;
        }

        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddParameter(new GH_PointCloudParam(), "Point Cloud", "C", "The reduced point cloud.", GH_ParamAccess.item);
        }

        protected override void SolveInstance(IGH_DataAccess DA)
        {
            PointCloud cloud = new PointCloud();
            double fraction = 0.5;
            int seed = 2;
            if (!DA.GetData("Point Cloud", ref cloud)) return;
            if (!DA.GetData("Fraction", ref fraction)) return;
            DA.GetData("Seed", ref seed);


            if (fraction < 0 || fraction > 1)
            {
                AddRuntimeMessage(GH_RuntimeMessageLevel.Error, "Fraction should be between 0 and 1");
            }

            PointCloud pc = new PointCloud();
            Random random = new Random(seed);

            for (int i = 0; i < cloud.Count; i++)
            {
                if (random.NextDouble() < fraction)
                {
                    var itm = cloud[i];
                    pc.Add(itm.Location, itm.Normal, itm.Color);
                }
            }


            DA.SetData("Point Cloud", new GH_PointCloud(pc));
        }

        protected override System.Drawing.Bitmap Icon
        {
            get
            {
                return Tarsier.Properties.Resources.icon_pointcloudsubset;
            }
        }

        public override Guid ComponentGuid
        {
            get { return new Guid("{D232F07D-7E43-4534-9807-E70C9D7A8755}"); }
        }
    }
}
