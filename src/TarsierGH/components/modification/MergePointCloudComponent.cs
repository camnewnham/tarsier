﻿/*
 *     __                      .__              
 *   _/  |______ _______  _____|__| ___________ 
 *   \   __\__  \\_  __ \/  ___/  |/ __ \_  __ \
 *    |  |  / __ \|  | \/\___ \|  \  ___/|  | \/
 *    |__| (____  /__|  /____  >__|\___  >__|   
 *              \/           \/        \/       
 * 
 *    Copyright Cameron Newnham 2015-2016
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;

using Grasshopper.Kernel;
using Rhino.Geometry;
using TarsierGH;

namespace Tarsier.Tools
{
    public class MergePointCloudComponent : GH_Component
    {
        public MergePointCloudComponent()
          : base("Merge Point Clouds", "Merge",
              "Merges multiple point clouds into one.",
              "Tarsier", "Modify")
        {
        }

        public override GH_Exposure Exposure
        {
            get
            {
                return GH_Exposure.quinary;
            }
        }

        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddParameter(new GH_PointCloudParam(), "Point Clouds", "C", "The the point clouds to merge", GH_ParamAccess.list);
        }

        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddParameter(new GH_PointCloudParam(), "Point Cloud", "C", "The merged point cloud", GH_ParamAccess.item);
        }

        protected override void SolveInstance(IGH_DataAccess DA)
        {
            List<GH_PointCloud> clouds = new List<GH_PointCloud>();
            if (!DA.GetDataList("Point Clouds", clouds)) return;

            if (clouds.Count == 1)
            {
                AddRuntimeMessage(GH_RuntimeMessageLevel.Warning, "Only one cloud was provided, so no merge was executed.");
                DA.SetData("Point Cloud", clouds[0].Duplicate());
            } else if (clouds.Count > 1)
            {
                var pc = new PointCloud();
                foreach (var opc in clouds)
                {
                    pc.Merge(opc.Value);
                }
                DA.SetData("Point Cloud", new GH_PointCloud(pc));
            }
        }

        protected override System.Drawing.Bitmap Icon
        {
            get
            {
                return Tarsier.Properties.Resources.icon_mergepointclouds;
            }
        }

        public override Guid ComponentGuid
        {
            get { return new Guid("{80B3E6B6-54D7-40B7-85CE-E797A8C47B4B}"); }
        }
    }
}
