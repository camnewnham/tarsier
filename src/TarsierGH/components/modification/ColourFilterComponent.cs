﻿/*
 *     __                      .__              
 *   _/  |______ _______  _____|__| ___________ 
 *   \   __\__  \\_  __ \/  ___/  |/ __ \_  __ \
 *    |  |  / __ \|  | \/\___ \|  \  ___/|  | \/
 *    |__| (____  /__|  /____  >__|\___  >__|   
 *              \/           \/        \/       
 * 
 *    Copyright Cameron Newnham 2015-2016
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;

using Grasshopper.Kernel;
using Rhino.Geometry;
using System.Drawing;
using TarsierGH;
using System.Windows.Forms;

namespace Tarsier.Tools
{
    public class ColourFilterComponent : GH_Component
    {
        public ColourFilterComponent()
          : base("Filter Point Cloud", "Filter",
              "Filters a point cloud by removing values similar to a given colour.",
              "Tarsier", "Modify")
        {
        }

        public override GH_Exposure Exposure
        {
            get
            {
                return GH_Exposure.quinary;
            }
        }

        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddParameter(new GH_PointCloudParam(), "Point Cloud", "C", "The point cloud to filter.", GH_ParamAccess.item);
            pManager.AddColourParameter("Colours", "C", "The colours to filter.", GH_ParamAccess.list);
            pManager.AddNumberParameter("Thresholds", "T", "The thresholds for each colour.", GH_ParamAccess.list, .1);
            pManager[2].Optional = true;
        }

        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddParameter(new GH_PointCloudParam(), "Point Cloud", "C", "The cropped point cloud.", GH_ParamAccess.item);
        }

        double max = 255 * 3;

        System.Threading.Tasks.ParallelOptions pOpt = new System.Threading.Tasks.ParallelOptions
        {
            MaxDegreeOfParallelism = System.Environment.ProcessorCount * 2
        };

        protected override void SolveInstance(IGH_DataAccess DA)
        {
            PointCloud cloud = new PointCloud();
            List<Color> colors = new List<Color>();
            List<double> thresh = new List<double>();

            if (!DA.GetData("Point Cloud", ref cloud)) return;
            if (!DA.GetDataList("Colours", colors)) return;
            if (!DA.GetDataList("Thresholds", thresh)) return;

            PointCloud npc = new PointCloud();

            bool outOfTol = false;
            List<double> dif = new List<double>();
            if (thresh.Count == colors.Count)
            {
                for (int i = 0; i < colors.Count; i++) {
                    var num = thresh[i];
                    if (num < 0 || num > 1) outOfTol = true;
                    dif.Add(num * max);
                }
            }
            else if (thresh.Count != colors.Count)
            {
                if (thresh.Count != 1)
                    AddRuntimeMessage(GH_RuntimeMessageLevel.Warning, "Threshold count should match color count.");
                for (int i = 0; i < colors.Count; i++)
                {
                    var num = thresh[0];
                    if (num < 0 || num > 1) outOfTol = true;
                    dif.Add(num * max);
                }
            }
            if (outOfTol) AddRuntimeMessage(GH_RuntimeMessageLevel.Warning, "Threshold should be between 0 and 1");

            System.Threading.Tasks.Parallel.ForEach(cloud, pOpt, p => {
                int avail = 0;
                int cc = 0;
                foreach (var color in colors)
                {
                    int sum = 0;
                    sum += (Math.Abs(color.R - p.Color.R));
                    sum += (Math.Abs(color.G - p.Color.G));
                    sum += (Math.Abs(color.B - p.Color.B));

                    if (sum > dif[cc])
                    {
                        avail++;
                    }
                    
                    cc++;
                }
                if ((avail == colors.Count && !invert) || (avail != colors.Count && invert))
                {
                    lock (npc) npc.Add(p.Location, p.Normal, p.Color);
                }
            });

            DA.SetData("Point Cloud", new GH_PointCloud(npc));
        }

        protected override System.Drawing.Bitmap Icon
        {
            get
            {
                return Tarsier.Properties.Resources.icon_colourfilter;
            }
        }

        public override Guid ComponentGuid
        {
            get { return new Guid("{350206E5-4CA5-4413-8490-EB85231533AD}"); }
        }

        private bool invert = false;

        #region DepthUserOptions
        protected override void AppendAdditionalComponentMenuItems(ToolStripDropDown menu)
        {
            var item0 = Menu_AppendItem(menu, "Invert", Menu_OptionToggled, true, invert);
            item0.ToolTipText = "Invert the filter.";
        }

        private void Menu_OptionToggled(object sender, EventArgs args)
        {
            ToolStripMenuItem item = (ToolStripMenuItem)sender;
            if (item.Text == "Invert")
            {
                invert = !invert;
                item.Checked = invert;
            }
        }
        
        public override bool Write(GH_IO.Serialization.GH_IWriter writer)
        {
            writer.SetBoolean("invert", invert);
            return base.Write(writer);
        }
        public override bool Read(GH_IO.Serialization.GH_IReader reader)
        {
            invert = reader.GetBoolean("invert");
            return base.Read(reader);
        }

        #endregion
    }
}
