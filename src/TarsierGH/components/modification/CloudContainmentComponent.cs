﻿/*
 *     __                      .__              
 *   _/  |______ _______  _____|__| ___________ 
 *   \   __\__  \\_  __ \/  ___/  |/ __ \_  __ \
 *    |  |  / __ \|  | \/\___ \|  \  ___/|  | \/
 *    |__| (____  /__|  /____  >__|\___  >__|   
 *              \/           \/        \/       
 * 
 *    Copyright Cameron Newnham 2015-2016
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;

using Grasshopper.Kernel;
using Rhino.Geometry;
using TarsierGH;

namespace Tarsier.Tools
{
    public class ContainCloudComponent : GH_Component
    {
        public ContainCloudComponent()
          : base("Clip Point Cloud", "Clip",
              "Clips a point cloud with a given box.",
              "Tarsier", "Modify")
        {
        }

        public override GH_Exposure Exposure
        {
            get
            {
                return GH_Exposure.quinary;
            }
        }

        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddParameter(new GH_PointCloudParam(), "Point Cloud", "C", "The point cloud to clip", GH_ParamAccess.item);
            pManager.AddBoxParameter("Clipping Box", "B", "The box to clip the point cloud", GH_ParamAccess.item);
        }

        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddParameter(new GH_PointCloudParam(), "Point Cloud", "C", "The cropped point cloud.", GH_ParamAccess.item);
        }

        System.Threading.Tasks.ParallelOptions pOpt = new System.Threading.Tasks.ParallelOptions
        {
            MaxDegreeOfParallelism = System.Environment.ProcessorCount * 2
        };

        protected override void SolveInstance(IGH_DataAccess DA)
        {
            PointCloud cloud = new PointCloud();
            Box box = new Box();
            if (!DA.GetData("Point Cloud", ref cloud)) return;
            if (!DA.GetData("Clipping Box", ref box)) return;

            PointCloud newPC = new PointCloud();
            System.Threading.Tasks.Parallel.ForEach(cloud, pOpt, item =>
            {
                //foreach (PointCloudItem item in cloud.AsEnumerable())
                //{
                if (box.Contains(item.Location))
                {
                    lock (newPC) newPC.Add(item.Location, item.Normal, item.Color);
                }
            });

            
            DA.SetData("Point Cloud", new GH_PointCloud(newPC));
        }

        protected override System.Drawing.Bitmap Icon
        {
            get
            {
                return Tarsier.Properties.Resources.icon_clippointcloud;
            }
        }

        public override Guid ComponentGuid
        {
            get { return new Guid("{FA57AB90-ED76-4C77-9292-FE44BD6AA674}"); }
        }
    }
}
