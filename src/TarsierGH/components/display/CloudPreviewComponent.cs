﻿/*
 *     __                      .__              
 *   _/  |______ _______  _____|__| ___________ 
 *   \   __\__  \\_  __ \/  ___/  |/ __ \_  __ \
 *    |  |  / __ \|  | \/\___ \|  \  ___/|  | \/
 *    |__| (____  /__|  /____  >__|\___  >__|   
 *              \/           \/        \/       
 * 
 *    Copyright Cameron Newnham 2015-2016
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;

using Grasshopper.Kernel;
using Rhino.Geometry;
using TarsierGH;
using Grasshopper.Kernel.Data;

namespace Tarsier.Tools
{
    public class CloudPreviewComponent : GH_Component, IGH_PreviewObject
    {
        public CloudPreviewComponent()
          : base("Preview Point Cloud", "Preview",
              "Displays a point cloud with a specified point size.",
              "Tarsier", "Display")
        {
        }

        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddParameter(new GH_PointCloudParam(), "Point Cloud", "C", "The point cloud to preview.", GH_ParamAccess.tree);
            pManager.AddIntegerParameter("Weight", "W", "The point size for preview display.", GH_ParamAccess.item);
            pManager[0].Optional = true;
            pManager[1].Optional = true;
        }

        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
        }

        private int weight;
        private List<PointCloud> clouds;

        protected override void SolveInstance(IGH_DataAccess DA)
        {
            clouds = new List<PointCloud>();
            weight = 1;
            GH_Structure<GH_PointCloud> cloudTree;
            DA.GetDataTree<GH_PointCloud>("Point Cloud", out cloudTree);
            foreach (var pc in cloudTree.AllData(true))
            {
                clouds.Add(((GH_PointCloud) pc).Value);
            }
            DA.GetData("Weight", ref weight);
        }

        protected override System.Drawing.Bitmap Icon
        {
            get
            {
                return Tarsier.Properties.Resources.icon_pointcloudpreview;
            }
        }

        public override Guid ComponentGuid
        {
            get { return new Guid("{61D96D3C-1C99-4E61-8FA8-03DB1A6C71B9}"); }
        }

        public override void DrawViewportWires(IGH_PreviewArgs args)
        {
            if (!this.Locked)
            {
                if (clouds != null)
                {
                    foreach (var cloud in clouds)
                        args.Display.DrawPointCloud(cloud, weight);
                }
            }
            base.DrawViewportWires(args);

        }
    }
}
